package main

import (
	"aoc2022/days"
	"aoc2022/helper"
)

func main() {
	helper.PrintSolutions(days.NewDay01, "inputFiles/input01.txt")
	helper.PrintSolutions(days.NewDay02, "inputFiles/input02.txt")
	helper.PrintSolutions(days.NewDay03, "inputFiles/input03.txt")
	helper.PrintSolutions(days.NewDay04, "inputFiles/input04.txt")
	helper.PrintSolutions(days.NewDay05, "inputFiles/input05.txt")
	helper.PrintSolutions(days.NewDay06, "inputFiles/input06.txt")
	helper.PrintSolutions(days.NewDay07, "inputFiles/input07.txt")
	helper.PrintSolutions(days.NewDay08, "inputFiles/input08.txt")
	helper.PrintSolutions(days.NewDay09, "inputFiles/input09.txt")
}
