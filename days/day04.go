package days

import (
	"aoc2022/helper"
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type day04 struct {
	numberOfContainingPairs  uint64
	numberOfOverlappingPairs uint64
}

func (*day04) Number() uint8 {
	return 4
}

func NewDay04(filepath string) (*day04, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	bounds := [4]uint64{}
	var numberOfContainingPairs uint64 = 0
	var numberOfOverlappingPairs uint64 = 0

	for fileScanner.Scan() {
		lineSplit := strings.Split(strings.ReplaceAll(fileScanner.Text(), "-", ","), ",")
		if len(lineSplit) != 4 {
			return nil, errors.New("incorrectly formatted line")
		}

		for i := 0; i < 4; i++ {
			if num, err := strconv.ParseUint(lineSplit[i], 10, 64); err != nil {
				return nil, err
			} else {
				bounds[i] = num
			}
		}

		if (bounds[0] <= bounds[2] && bounds[3] <= bounds[1]) || (bounds[2] <= bounds[0] && bounds[1] <= bounds[3]) {
			numberOfContainingPairs += 1
		}

		if bounds[0] <= bounds[3] && bounds[2] <= bounds[1] {
			numberOfOverlappingPairs += 1
		}
	}

	return &day04{numberOfContainingPairs, numberOfOverlappingPairs}, nil
}

func (d *day04) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.numberOfContainingPairs)), nil
}

func (d *day04) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.numberOfOverlappingPairs)), nil
}
