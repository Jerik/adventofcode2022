package days

import (
	"aoc2022/helper"
	"bufio"
	"fmt"
	"os"
	"strings"
)

type day03 struct {
	sumOfDuplicatePriorities uint64
	sumOfBadgePriorities     uint64
}

func (*day03) Number() uint8 {
	return 3
}

func NewDay03(filepath string) (*day03, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	currentGroupRucksacks := make([]string, 0, 3)
	var sumOfDuplicatePriorities uint64 = 0
	var sumOfBadgePriorities uint64 = 0

	for fileScanner.Scan() {
		rucksack := fileScanner.Text()

		compartmentSize := len(rucksack) / 2
		for _, character := range rucksack[:compartmentSize] {
			if strings.ContainsRune(rucksack[compartmentSize:], character) {
				sumOfDuplicatePriorities += uint64(strings.IndexRune(helper.FullAlphabet, character) + 1)
				break
			}
		}

		currentGroupRucksacks = append(currentGroupRucksacks, rucksack)
		if len(currentGroupRucksacks) == 3 {
			for _, character := range currentGroupRucksacks[0] {
				if strings.ContainsRune(currentGroupRucksacks[1], character) {
					if strings.ContainsRune(currentGroupRucksacks[2], character) {
						sumOfBadgePriorities += uint64(strings.IndexRune(helper.FullAlphabet, character) + 1)
						currentGroupRucksacks = currentGroupRucksacks[:0]
						break
					}
				}
			}
		}
	}

	return &day03{sumOfDuplicatePriorities, sumOfBadgePriorities}, nil
}

func (d *day03) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.sumOfDuplicatePriorities)), nil
}

func (d *day03) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.sumOfBadgePriorities)), nil
}
