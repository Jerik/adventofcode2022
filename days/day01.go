package days

import (
	"aoc2022/helper"
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

type day01 struct {
	maxElfCalories [3]uint64
}

func (*day01) Number() uint8 {
	return 1
}

func NewDay01(filepath string) (*day01, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	var currentElfCalories uint64 = 0
	maxElfCalories := [3]uint64{}

	for fileScanner.Scan() {
		if line := fileScanner.Text(); line != "" {
			if calories, err := strconv.ParseUint(line, 10, 64); err != nil {
				return nil, err
			} else {
				currentElfCalories += calories
			}
		} else {
			if currentElfCalories > maxElfCalories[0] {
				maxElfCalories[0] = currentElfCalories
				sort.Slice(maxElfCalories[:], func(i, j int) bool {
					return maxElfCalories[i] < maxElfCalories[j]
				})
			}

			currentElfCalories = 0
		}
	}

	return &day01{maxElfCalories}, nil
}

func (d *day01) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.maxElfCalories[2])), nil
}

func (d *day01) Part2Solution() (helper.Solution, error) {
	totalMaxElfCalories := d.maxElfCalories[0] + d.maxElfCalories[1] + d.maxElfCalories[2]
	return helper.Solution(fmt.Sprint(totalMaxElfCalories)), nil
}
