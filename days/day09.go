package days

import (
	"aoc2022/helper"
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type position struct {
	x int64
	y int64
}

type day09 struct {
	visitedPositionsOfShortTail map[position]bool
	visitedPositionsOfLongTail  map[position]bool
}

func (*day09) Number() uint8 {
	return 9
}

func NewDay09(filepath string) (*day09, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	ropePositions := make([]position, 10)
	visitedPositionsOfShortTail := make(map[position]bool)
	visitedPositionsOfLongTail := make(map[position]bool)

	visitedPositionsOfShortTail[ropePositions[1]] = true
	visitedPositionsOfLongTail[ropePositions[9]] = true

	for fileScanner.Scan() {
		lineSplit := strings.Split(fileScanner.Text(), " ")
		if len(lineSplit) != 2 {
			return nil, errors.New("incorrectly formatted line")
		}

		var headCoordinate *int64 = nil
		var headDirection int64 = 0

		switch lineSplit[0] {
		case "R":
			headCoordinate, headDirection = &ropePositions[0].x, 1
		case "L":
			headCoordinate, headDirection = &ropePositions[0].x, -1
		case "U":
			headCoordinate, headDirection = &ropePositions[0].y, 1
		case "D":
			headCoordinate, headDirection = &ropePositions[0].y, -1
		default:
			return nil, errors.New("incorrectly formatted line")
		}

		steps, err := strconv.ParseInt(lineSplit[1], 10, 0)
		if err != nil {
			return nil, err
		}

		for i := 0; i < int(steps); i++ {
			*headCoordinate += headDirection

			for j := 1; j < 10; j++ {
				xDiff := ropePositions[j-1].x - ropePositions[j].x
				yDiff := ropePositions[j-1].y - ropePositions[j].y

				absDiffSum := helper.AbsInt(xDiff) + helper.AbsInt(yDiff)
				correction := (absDiffSum % 2) * (absDiffSum - 2)

				ropePositions[j].x += (xDiff + correction*helper.SignInt(xDiff)) / 2
				ropePositions[j].y += (yDiff + correction*helper.SignInt(yDiff)) / 2
			}

			visitedPositionsOfShortTail[ropePositions[1]] = true
			visitedPositionsOfLongTail[ropePositions[9]] = true
		}
	}

	return &day09{visitedPositionsOfShortTail, visitedPositionsOfLongTail}, nil
}

func (d *day09) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(len(d.visitedPositionsOfShortTail))), nil
}

func (d *day09) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(len(d.visitedPositionsOfLongTail))), nil
}
