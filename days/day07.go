package days

import (
	"aoc2022/helper"
	"bufio"
	"errors"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type directory struct {
	parent    *directory
	children  map[string]*directory
	fileSizes map[string]uint64
}

func (dir *directory) totalSizes(totalSizes []uint64) []uint64 {
	var totalSize uint64 = 0

	for _, child := range dir.children {
		totalSizes = child.totalSizes(totalSizes)
		totalSize += totalSizes[len(totalSizes)-1]
	}

	for _, fileSize := range dir.fileSizes {
		totalSize += fileSize
	}

	return append(totalSizes, totalSize)
}

type day07 struct {
	sumOfSmallTotalSizes  uint64
	minDeletableTotalSize uint64
}

func (*day07) Number() uint8 {
	return 7
}

func NewDay07(filepath string) (*day07, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	numberOfDirectories := 1
	rootDirectory := directory{nil, make(map[string]*directory), make(map[string]uint64)}
	currentDirectory := &rootDirectory

	for fileScanner.Scan() {
		lineSplit := strings.Split(fileScanner.Text(), " ")
		if len(lineSplit) < 2 || (lineSplit[1] == "cd" && len(lineSplit) < 3) {
			return nil, errors.New("incorrectly formatted line")
		}

		switch lineSplit[0] {
		case "$":
			if lineSplit[1] == "cd" {
				switch lineSplit[2] {
				case "/":
					currentDirectory = &rootDirectory
				case "..":
					currentDirectory = currentDirectory.parent
				default:
					currentDirectory = currentDirectory.children[lineSplit[2]]
				}
			}
		case "dir":
			if _, ok := currentDirectory.children[lineSplit[1]]; !ok {
				currentDirectory.children[lineSplit[1]] = &directory{
					parent:    currentDirectory,
					children:  make(map[string]*directory),
					fileSizes: make(map[string]uint64),
				}
				numberOfDirectories += 1
			}
		default:
			if fileSize, err := strconv.ParseUint(lineSplit[0], 10, 64); err != nil {
				return nil, err
			} else if _, ok := currentDirectory.fileSizes[lineSplit[1]]; !ok {
				currentDirectory.fileSizes[lineSplit[1]] = fileSize
			}
		}
	}

	totalSizes := rootDirectory.totalSizes(make([]uint64, 0, numberOfDirectories))
	unusedSpace := 70000000 - totalSizes[len(totalSizes)-1]
	var sumOfSmallTotalSizes uint64 = 0
	var minDeletableTotalSize uint64 = math.MaxUint64

	for _, totalSize := range totalSizes {
		if totalSize <= 100000 {
			sumOfSmallTotalSizes += totalSize
		}

		if totalSize < minDeletableTotalSize && unusedSpace+totalSize >= 30000000 {
			minDeletableTotalSize = totalSize
		}
	}

	return &day07{sumOfSmallTotalSizes, minDeletableTotalSize}, nil
}

func (d *day07) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.sumOfSmallTotalSizes)), nil
}

func (d *day07) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.minDeletableTotalSize)), nil
}
