package days

import (
	"aoc2022/helper"
	"bufio"
	"errors"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type stack = []rune

type day05 struct {
	crateMover9000Stacks []stack
	crateMover9001Stacks []stack
}

func (*day05) Number() uint8 {
	return 5
}

func NewDay05(filepath string) (*day05, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	maxStackIndex := -1
	crateMover9000Stacks := make([]stack, 0)
	crateMover9001Stacks := make([]stack, 0)

	for fileScanner.Scan() {
		line := fileScanner.Text()
		if line == "" {
			break
		}

		for i, character := range line {
			if unicode.IsLetter(character) {
				stackIndex := i / 4
				for maxStackIndex < stackIndex {
					crateMover9000Stacks = append(crateMover9000Stacks, make(stack, 0))
					crateMover9001Stacks = append(crateMover9001Stacks, make(stack, 0))
					maxStackIndex += 1
				}

				crateMover9000Stacks[stackIndex] = append(crateMover9000Stacks[stackIndex], character)
				crateMover9001Stacks[stackIndex] = append(crateMover9001Stacks[stackIndex], character)
			}
		}
	}

	for i := 0; i <= maxStackIndex; i++ {
		helper.ReverseSlice(crateMover9000Stacks[i])
		helper.ReverseSlice(crateMover9001Stacks[i])
	}

	for fileScanner.Scan() {
		lineSplit := strings.Split(fileScanner.Text(), " ")
		if len(lineSplit) < 6 {
			return nil, errors.New("incorrectly formatted line")
		}

		amount, err := strconv.ParseUint(lineSplit[1], 10, 64)
		if err != nil {
			return nil, err
		}
		src, err := strconv.ParseUint(lineSplit[3], 10, 64)
		if err != nil {
			return nil, err
		}
		dest, err := strconv.ParseUint(lineSplit[5], 10, 64)
		if err != nil {
			return nil, err
		}

		src -= 1
		dest -= 1
		crateIndex := len(crateMover9000Stacks[src])

		for amount > 0 {
			amount -= 1
			crateIndex -= 1
			crateMover9000Stacks[dest] = append(crateMover9000Stacks[dest], crateMover9000Stacks[src][crateIndex])
			crateMover9000Stacks[src] = crateMover9000Stacks[src][:crateIndex]
		}

		crateMover9001Stacks[dest] = append(crateMover9001Stacks[dest], crateMover9001Stacks[src][crateIndex:]...)
		crateMover9001Stacks[src] = crateMover9001Stacks[src][:crateIndex]
	}

	return &day05{crateMover9000Stacks, crateMover9001Stacks}, nil
}

func topCratesSolution(stacks []stack) (helper.Solution, error) {
	topCrates := make([]rune, len(stacks))

	for i, stack := range stacks {
		if len(stack) == 0 {
			return helper.Solution(""), errors.New("empty stack")
		}

		topCrates[i] = stack[len(stack)-1]
	}

	return helper.Solution(string(topCrates)), nil
}

func (d *day05) Part1Solution() (helper.Solution, error) {
	return topCratesSolution(d.crateMover9000Stacks)
}

func (d *day05) Part2Solution() (helper.Solution, error) {
	return topCratesSolution(d.crateMover9001Stacks)
}
