package days

import (
	"aoc2022/helper"
	"fmt"
	"os"
)

type day06 struct {
	firstPacketStart  uint64
	firstMessageStart uint64
}

func (*day06) Number() uint8 {
	return 6
}

func NewDay06(filepath string) (*day06, error) {
	datastream, err := os.ReadFile(filepath)
	if err != nil {
		return nil, err
	}

	bufferWindow := make([]rune, 0, 14)
	var firstPacketStart uint64 = 0
	var firstMessageStart uint64 = 0

	for i, character := range string(datastream) {
		for j := len(bufferWindow) - 1; j >= 0; j-- {
			if bufferWindow[j] == character {
				bufferWindow = bufferWindow[j+1:]
				break
			}
		}

		bufferWindow = append(bufferWindow, character)

		if firstPacketStart == 0 && len(bufferWindow) == 4 {
			firstPacketStart = uint64(i + 1)
		}

		if len(bufferWindow) == 14 {
			firstMessageStart = uint64(i + 1)
			break
		}
	}

	return &day06{firstPacketStart, firstMessageStart}, nil
}

func (d *day06) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.firstPacketStart)), nil
}

func (d *day06) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.firstMessageStart)), nil
}
