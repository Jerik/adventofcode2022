package days

import (
	"aoc2022/helper"
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
)

type day02 struct {
	strategy1Score uint64
	strategy2Score uint64
}

func (*day02) Number() uint8 {
	return 2
}

func NewDay02(filepath string) (*day02, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	moveMap := map[string]uint64{
		"A": 1, "B": 2, "C": 3,
		"X": 1, "Y": 2, "Z": 3,
	}
	var strategy1Score uint64 = 0
	var strategy2Score uint64 = 0

	for fileScanner.Scan() {
		before, after, found := strings.Cut(fileScanner.Text(), " ")
		opponentMove, ok1 := moveMap[before]
		playerMove, ok2 := moveMap[after]
		if !found || !ok1 || !ok2 {
			return nil, errors.New("incorrectly formatted line")
		}

		strategy1Score += playerMove
		strategy1Score += ((4 + playerMove - opponentMove) % 3) * 3

		strategy2Score += (playerMove+opponentMove)%3 + 1
		strategy2Score += (playerMove - 1) * 3
	}

	return &day02{strategy1Score, strategy2Score}, nil
}

func (d *day02) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.strategy1Score)), nil
}

func (d *day02) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.strategy2Score)), nil
}
