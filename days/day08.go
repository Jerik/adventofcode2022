package days

import (
	"aoc2022/helper"
	"bufio"
	"errors"
	"fmt"
	"os"
)

type tree struct {
	height      uint8
	scenicScore uint64
	isVisible   bool
}

func newTree(height uint8) tree {
	return tree{height: height, scenicScore: 1, isVisible: false}
}

type treeHistory struct {
	lastBlockingOccurences []uint64
	currentOccurence       uint64
}

func newTreeHistory() treeHistory {
	return treeHistory{
		lastBlockingOccurences: make([]uint64, 10),
		currentOccurence:       1,
	}
}

func (hist *treeHistory) reset() {
	for h := 0; h < 10; h++ {
		hist.lastBlockingOccurences[h] = 0
	}

	hist.currentOccurence = 1
}

func (hist *treeHistory) add(currentTree *tree) {
	lastBlockingOccurence := hist.lastBlockingOccurences[currentTree.height]

	if lastBlockingOccurence > 0 {
		currentTree.scenicScore *= hist.currentOccurence - lastBlockingOccurence
	} else {
		currentTree.scenicScore *= hist.currentOccurence - 1
		currentTree.isVisible = true
	}

	for h := 0; h <= int(currentTree.height); h++ {
		hist.lastBlockingOccurences[h] = hist.currentOccurence
	}

	hist.currentOccurence += 1
}

type day08 struct {
	numberOfVisibleTrees uint64
	maxScenicScore       uint64
}

func (*day08) Number() uint8 {
	return 8
}

func NewDay08(filepath string) (*day08, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	defer file.Close()
	fileScanner := bufio.NewScanner(file)

	treeGrid := make([][]tree, 0)
	rowLength := 0

	for i := 0; fileScanner.Scan(); i++ {
		line := fileScanner.Text()
		treeGrid = append(treeGrid, make([]tree, 0, rowLength))

		for _, character := range line {
			height := uint8(character - '0')
			if height > 9 {
				return nil, errors.New("invalid character")
			}

			treeGrid[i] = append(treeGrid[i], newTree(height))
		}

		if i == 0 {
			rowLength = len(treeGrid[0])
		} else if len(treeGrid[i]) != rowLength {
			return nil, errors.New("lines of different lengths")
		}
	}

	columnLength := len(treeGrid)
	forwardHistory := newTreeHistory()
	backwardHistory := newTreeHistory()

	for i := 0; i < columnLength; i++ {
		forwardHistory.reset()
		backwardHistory.reset()
		for j := 0; j < rowLength; j++ {
			forwardHistory.add(&treeGrid[i][j])
			backwardHistory.add(&treeGrid[i][rowLength-1-j])
		}
	}

	for j := 0; j < rowLength; j++ {
		forwardHistory.reset()
		backwardHistory.reset()
		for i := 0; i < columnLength; i++ {
			forwardHistory.add(&treeGrid[i][j])
			backwardHistory.add(&treeGrid[columnLength-1-i][j])
		}
	}

	var numberOfVisibleTrees uint64 = 0
	var maxScenicScore uint64 = 0

	for i := 0; i < columnLength; i++ {
		for j := 0; j < rowLength; j++ {
			currentTree := &treeGrid[i][j]

			if currentTree.isVisible {
				numberOfVisibleTrees += 1
			}

			if currentTree.scenicScore > maxScenicScore {
				maxScenicScore = currentTree.scenicScore
			}
		}
	}

	return &day08{numberOfVisibleTrees, maxScenicScore}, nil
}

func (d *day08) Part1Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.numberOfVisibleTrees)), nil
}

func (d *day08) Part2Solution() (helper.Solution, error) {
	return helper.Solution(fmt.Sprint(d.maxScenicScore)), nil
}
