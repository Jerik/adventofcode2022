package helper

import (
	"fmt"
	"log"
)

type Solution string

type Day interface {
	Number() uint8
	Part1Solution() (Solution, error)
	Part2Solution() (Solution, error)
}

func PrintSolutions[D Day, I any](newDay func(I) (D, error), input I) {
	day, err := newDay(input)
	if err != nil {
		log.Printf("Error while processing Day %02d input: %v\n", day.Number(), err)
		return
	}

	fmt.Printf("Day %02d Solutions:\n", day.Number())

	if solution, err := day.Part1Solution(); err != nil {
		log.Printf("Error while solving Part 1: %v\n", err)
	} else {
		fmt.Printf("- Part 1: %v\n", solution)
	}

	if solution, err := day.Part2Solution(); err != nil {
		log.Printf("Error while solving Part 2: %v\n", err)
	} else {
		fmt.Printf("- Part 2: %v\n", solution)
	}

	fmt.Println()
}
