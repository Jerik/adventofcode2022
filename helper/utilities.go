package helper

const SmallAlphabet string = "abcdefghijklmnopqrstuvwxyz"
const LargeAlphabet string = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const FullAlphabet string = SmallAlphabet + LargeAlphabet

func ReverseSlice[T any](slice []T) {
	length := len(slice)
	for i := 0; i < length/2; i++ {
		slice[i], slice[length-1-i] = slice[length-1-i], slice[i]
	}
}

func AbsInt[T ~int | ~int8 | ~int16 | ~int32 | ~int64](value T) T {
	if value < 0 {
		return -value
	}

	return value
}

func SignInt[T ~int | ~int8 | ~int16 | ~int32 | ~int64](value T) T {
	if value < 0 {
		return -1
	} else if value > 0 {
		return 1
	}

	return 0
}
